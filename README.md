# zTab

zTab是一个layui多标签页插件，仿照了layuiAdmin的iframe版Tab实现

##### 当前版本v1.0

在线预览：[http://47.96.138.120/manage/](http://47.96.138.120/manage/)

## TODO
- 添加主题配置项
- 自定义Tab样式配置项(包括常规样式、选中样式、hover样式等)
- 右键菜单功能

使用说明：

1、复制static/layui/mymodules下的zTab文件夹到你的layui第三方模块的目录中

2、在layui.js中加入如下代码

```
layui.config({
    base: '/static/layui/mymodules/' //假设这是你存放拓展模块的根目录
}).extend({ //设定模块别名
    tab: 'zTab/zTab'
});
```

3、html代码如下

```
<div class="layui-side layui-side-menu">
	<div class="layui-side-scroll">
		<div class="layui-logo">zTab</div>
		<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
		<ul class="layui-nav layui-nav-tree" lay-filter="test">
			<li class="layui-nav-item layui-nav-itemed">
				<a class="" href="javascript:void(0);" >后台管理</a>
				<dl class="layui-nav-child">
					<dd>
						<a lay-href="/manage/user.html" href="javascript:;">用户管理</a>
					</dd>
					<dd>
						<a lay-href="/manage/role.html" href="javascript:;">角色管理</a>
					</dd>
					<dd>
						<a lay-href="/manage/menu.html" href="javascript:;">权限管理</a>
					</dd>
				</dl>
			</li>
		</ul>
	</div>
</div>
<!-- Tab组所在元素 -->
<div id="tabs"></div>
<!-- Tab组内容所在元素 -->
<div id="tabs-body" class="layui-body">
```

4、js代码如下

```
layui.use('tab', function () {
	var tab = layui.tab;
	tab.init('#tabs', {
		index: {
			url: 'content_index.html'
		}
	});
	for(var i=0;i<50;i++){
		tab.addTab({
			id: i,
			title: 'Tab'+i,
			type: 'html',
			content: '<h1>aa'+ i +'</h1>',
			url: '/manage/user'+ i +'.html'
		});
	}
	// console.log("Tab初始化完成");
});
```
说明：
```
如Tab组所在的元素设置ID为tabs
则Tab内容所在的元素ID必须为tabs-body

组件会自动监听页面中带有lay-href属性的a标签的点击事件，自动创建新的Tab
id自动设为lay-href的值，类型为iframe
```



zTab函数说明：

```
init(select, option): 初始化函数
	参数说明：
	select: "#tabs"	//Tab组所在的元素	使用jquery选择器语法
	option: {	 //初始化参数
		index: {		//首页初始化参数
			id: "index",	//默认值为"index",不可修改
			close: false, //是否可关闭，默认为false
			type: 'iframe', //默认为iframe['text','html','iframe']
			title: '', //标题
			content: '', //内容 type为text、html时必填
			url: '', //url type为iframe时必填
			icon: 'layui-icon-home' //图标 目前仅支持layui图标
		}
	}
	
addTab(tab):	新增Tab页
	参数说明：
	tab: {
		id: "",	//唯一主键，不可为空
		close: true, //是否可关闭,默认为true
		type: 'iframe', //默认为iframe,['text','html','iframe']
		title: '', //标题
		content: '', //内容 type为text、html时必填
		url: '', //url type为iframe时必填
		icon: '' //图标 目前仅支持layui图标
	}
	
close(id):	关闭指定Tab
	参数说明：
	id: "tab1" //Tab id
	
closeAll():	关闭所有Tab(close为false的不会关闭)
	无参数

closeOthers(id):	关闭其他Tab(close为false的不会关闭)
	参数说明：
	id: "tab1" //Tab id
```